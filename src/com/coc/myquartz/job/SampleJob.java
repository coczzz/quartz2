package com.coc.myquartz.job;

import java.util.Date;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;


public class SampleJob implements Job/*StatefulJob*/{

	public void execute(JobExecutionContext context) throws JobExecutionException {
		Date date  = new Date();
		String time = date.toString();
		
		System.out.println(time + " : SampleJob is started ! -- 2" );
		
		try {
			Thread.sleep(10 * 1000);
		} catch (InterruptedException e) {		}
		
	}

}
