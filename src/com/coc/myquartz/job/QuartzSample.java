package com.coc.myquartz.job;

import java.util.Date;
import java.util.UUID;

import org.quartz.CronTrigger;
import org.quartz.DateIntervalTrigger;
import org.quartz.JobDetail;
import org.quartz.ObjectAlreadyExistsException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;

public class QuartzSample {
	
	private static SchedulerFactory sf = new StdSchedulerFactory();
	
	public void run() throws Exception {
		
		try {
		
			Scheduler sched = sf.getScheduler();
			CronTrigger trigger = new CronTrigger("SampleJob","SampleJob");
			trigger.setCronExpression("0/5 * * * * ?");// 触发器时间设定
			
	       
			JobDetail job = new JobDetail("SampleJob","SampleGroup1",SampleJob.class);
			
	//		final DateIntervalTrigger trigger = new DateIntervalTrigger();
	//		trigger.setName("sampleTrigger1");
	//		trigger.setStartTime(new Date());
	//		trigger.setRepeatIntervalUnit(DateIntervalTrigger.IntervalUnit.SECOND);
	//		trigger.setRepeatInterval(20);
			
		
			
			sched.scheduleJob(job, trigger);
			
			
			
	//		Thread.sleep(100 * 1000);
			
	//		sched.deleteJob("SampleJob", "SampleGroup");
			
	//		sched.shutdown(true);
			
			
		}catch (ObjectAlreadyExistsException e) {
			System.out.println("集群啓動會抛出磁異常，屬於正常現象！！！！，攔截不做任何處理，否則不能正常啓動！！！");
			 e.printStackTrace();
		} catch (SchedulerException e) {
	        e.printStackTrace();
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
		
		// sf.getScheduler().start();
	}
	
	public void startJobs() {
        try {
            Scheduler sched = sf.getScheduler();
            System.out.println(sched.getSchedulerName());
            sched.start();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
	

	public static void main(String[] args) throws Exception {
		QuartzSample sample = new QuartzSample();
//		sample.run();
//		sample.startJobs();
		sample.deleteJob();
	}
	
	
	public void deleteJob() throws Exception{
		SchedulerFactory sf = new StdSchedulerFactory();
		Scheduler sched = sf.getScheduler();
		sched.deleteJob("SampleJob", "SampleGroup1");
		
		sched.shutdown(true);
	}
	
	
}
